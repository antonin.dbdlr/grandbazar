-- V1__Create_articles_and_vendeur_tables.sql

CREATE TABLE IF NOT EXISTS `articles` (
    `idArticle` INT NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(45) DEFAULT NULL,
    `nomArticle` VARCHAR(45) DEFAULT NULL,
    `prixArticle` INT DEFAULT NULL,
    `idVendeur` INT NOT NULL,
    `motDePasse` VARCHAR(255),
    PRIMARY KEY (`idarticles`),
    KEY `fk_idvendeur` (`idVendeur`),
    CONSTRAINT `fk_idvendeur` FOREIGN KEY (`idVendeur`) REFERENCES `vendeur` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `vendeur` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `nom` VARCHAR(255) NOT NULL,
    `prenom` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
