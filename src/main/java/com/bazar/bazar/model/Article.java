package com.bazar.bazar.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@Entity
@ToString
@Getter
@Table(name = "articles") //nom de la table (par defaut le nom de la classe)
public class Article {
    @Id
    @Column(name = "idarticles")
    private int idArticles;

    @Column(name = "type")
    private String type;
    @Column(name = "nomArticle")
    private String nomArticle;
    @Column(name = "prixArticle")
    private int prixArticle;

    //@ManyToOne
    //@JoinColumn(name = "idvendeur", referencedColumnName = "id")
    //@JsonManagedReference
   // private int vendeur;

    @Column(name = "idvendeur")
    private int vendeurId;

    public Article() {
    }

    public Article(int idArticles, String type, String nomArticle, int prixArticle, int idVendeur) {
        this.idArticles = idArticles;
        this.type = type;
        this.nomArticle = nomArticle;
        this.prixArticle = prixArticle;
        this.vendeurId = idVendeur;
    }




}
