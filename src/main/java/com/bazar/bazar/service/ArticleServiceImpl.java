package com.bazar.bazar.service;

import com.bazar.bazar.model.Article;
import com.bazar.bazar.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public String addArticle(Article article) {
        articleRepository.save(article);
        return null;
    }

    @Override
    public void deleteArticle(int id) {
        articleRepository.deleteById(id);
    }

    @Override
    public Article updateArticle(int idArticles, Article article) {
        Article articleExistant = getArticleById(idArticles);
        articleExistant.setNomArticle(article.getNomArticle());
        articleExistant.setType(article.getType());
        articleExistant.setPrixArticle(article.getPrixArticle());
        return articleRepository.save(articleExistant);
    }

    @Override
    public List<Article> getAllArticles() {
        return articleRepository.findAll();
    }

    @Override
    public Article getArticleById(int id) {
        Optional<Article> article = articleRepository.findById(id);
        return article.orElseThrow(() -> new RuntimeException("Article non trouvé"));
    }


}
