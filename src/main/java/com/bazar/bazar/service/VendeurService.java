package com.bazar.bazar.service;

import com.bazar.bazar.model.Article;
import com.bazar.bazar.model.Vendeur;

import java.util.List;

public interface VendeurService {
    public String addVendeur(Vendeur vendeur);
    public void deleteVendeur(int id);
    public Vendeur updateVendeur(int id, Vendeur vendeur);
    public List<Vendeur> getAllVendeurs();
    public Vendeur getVendeurById(int id);

}
