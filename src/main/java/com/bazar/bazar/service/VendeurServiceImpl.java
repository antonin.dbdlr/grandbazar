package com.bazar.bazar.service;

import com.bazar.bazar.model.Article;
import com.bazar.bazar.model.Vendeur;
import com.bazar.bazar.repository.ArticleRepository;
import com.bazar.bazar.repository.VendeurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VendeurServiceImpl implements VendeurService {

    @Autowired
    private VendeurRepository vendeurRepository;

    @Override
    public String addVendeur(Vendeur vendeur) {
        vendeurRepository.save(vendeur);
        return null;
    }

    @Override
    public void deleteVendeur(int id) {
        vendeurRepository.deleteById(id);
    }

    @Override
    public Vendeur updateVendeur(int id, Vendeur vendeur) {
        Vendeur vendeurExistant = getVendeurById(id);
        vendeurExistant.setNom(vendeur.getNom());
        vendeurExistant.setPrenom(vendeur.getPrenom());
        vendeurExistant.setEmail(vendeur.getEmail());
        return vendeurRepository.save(vendeurExistant);
    }

    @Override
    public List<Vendeur> getAllVendeurs() {
        return vendeurRepository.findAll();
    }

    @Override
    public Vendeur getVendeurById(int id) {
        Optional<Vendeur> vendeur = vendeurRepository.findById(id);
        return vendeur.orElseThrow(() -> new RuntimeException("Vendeur non trouvé"));
    }


}
