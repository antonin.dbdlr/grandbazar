package com.bazar.bazar.service;

import com.bazar.bazar.model.Article;

import java.util.List;

public interface ArticleService  {
    public String addArticle(Article article);
    public void deleteArticle(int id);
    public Article updateArticle(int idArticles, Article article);
    public List<Article> getAllArticles();
    public Article getArticleById(int id);

}
