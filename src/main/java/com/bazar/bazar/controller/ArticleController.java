package com.bazar.bazar.controller;

import com.bazar.bazar.model.Article;
import com.bazar.bazar.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@RestController
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @PostMapping("/postArticles")
    public String postArticles(@RequestBody Article article) {
        return articleService.addArticle(article);
    }

    @GetMapping("/getArticleById")
    public Article getArticleById(@RequestParam(name = "id") int id) {
        return articleService.getArticleById(id);
    }

    @GetMapping("/getAllArticles")
    public List<Article> getAllArticles() {
        return articleService.getAllArticles();
    }

    @PutMapping("/putArticles")
    public String putArticles(@RequestBody Article article) {
        return articleService.updateArticle(article.getIdArticles(), article).toString();
    }

    @DeleteMapping("/deleteArticle")
    public String deleteArticle(@RequestParam(name = "nomArticle") int id) {
        articleService.deleteArticle(id);
        return "Article supprimé";
    }

}

