package com.bazar.bazar.controller;

import com.bazar.bazar.model.Article;
import com.bazar.bazar.model.Vendeur;
import com.bazar.bazar.service.VendeurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VendeurController {
    @Autowired
    private VendeurService vendeurService;

    @PostMapping("/postVendeur")
    public String postVendeur(@RequestBody Vendeur vendeur) {
        return vendeurService.addVendeur(vendeur);
    }


    @GetMapping("/getVendeurById")
    public Vendeur getVendeurById(@RequestParam(name = "id") int id) {
        return vendeurService.getVendeurById(id);
    }

    @GetMapping("/getAllVendeurs")
    public List<Vendeur> getAllVendeurs() {
        return vendeurService.getAllVendeurs();
    }

    @PutMapping("/putVendeur")
    public String putVendeur(@RequestBody Vendeur vendeur) {
        return vendeurService.updateVendeur(vendeur.getId(), vendeur).toString();
    }

    @DeleteMapping("/deleteVendeur")
    public String deleteVendeur(@RequestParam(name = "nom") int id) {
        vendeurService.deleteVendeur(id);
        return "Vendeur supprimé";
    }


}
