package com.bazar.bazar.repository;

import com.bazar.bazar.model.Vendeur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendeurRepository extends JpaRepository<Vendeur, Integer> {
}
